@extends('frontend.layouts.master')

@section('content')
    <main>
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="https://getbootstrap.com//docs/5.1/assets/brand/bootstrap-logo.svg" alt="" width="72" height="57">
            <h2>Laravel - Nicepay API V2</h2>
            <p class="lead">Lorem ipsum dolor sir amet consecteur adpising elit</p>
        </div>

        <div class="row g-5">
            <div class="col-md-5 col-lg-4 order-md-last">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-primary">Your cart</span>
                    <span class="badge bg-primary rounded-pill">3</span>
                </h4>
                <ul class="list-group mb-3">
                    <li class="list-group-item d-flex justify-content-between lh-sm">
                        <div>
                            <h6 class="my-0">Dispenser</h6>
                            <small class="text-muted">Dispenser</small>
                        </div>
                        <span class="text-muted">Rp 10.000</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-sm">
                        <div>
                            <h6 class="my-0">Kulkas</h6>
                            <small class="text-muted">Kulkas</small>
                        </div>
                        <span class="text-muted">Rp. 20.000</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-sm">
                        <div>
                            <h6 class="my-0">Kipas Angin</h6>
                            <small class="text-muted">Kipas</small>
                        </div>
                        <span class="text-muted">Rp. 5.000</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between bg-light">
                        <div class="text-success">
                            <h6 class="my-0">Promo</h6>
                            <small>OSONGKIR</small>
                        </div>
                        <span class="text-success">−Rp 2.000</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between">
                        <span>Total (IDR)</span>
                        <strong>Rp 33.000</strong>
                    </li>
                </ul>

                @if( 1 == 2 )
                    <form class="card p-2">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Promo code">
                            <button type="submit" class="btn btn-secondary">Redeem</button>
                        </div>
                    </form>
                @endif
            </div>
            <div class="col-md-7 col-lg-8">
                <h4 class="mb-3">Billing address</h4>
                <form class="needs-validation" novalidate>
                    <div class="row g-3">
                        <div class="col-sm-6">
                            <label for="firstName" class="form-label">First name</label>
                            <input type="text" class="form-control" id="firstName" placeholder="" value="Deny" required>
                            <div class="invalid-feedback">
                                Valid first name is required.
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label for="lastName" class="form-label">Last name <span class="text-muted">(Optional)</span></label>
                            <input type="text" class="form-control" id="lastName" placeholder="" value="Suseno">
                            <div class="invalid-feedback">
                                Valid last name is required.
                            </div>
                        </div>

                        <div class="col-12">
                            <label for="phone" class="form-label">Phone</label>
                            <input type="text" class="form-control" id="phone" placeholder="08xxxxxxx" value="089628826333" required>
                            <div class="invalid-feedback">
                                Please enter your phone number.
                            </div>
                        </div>

                        @if( 1 == 2 )
                            <div class="col-12">
                                <label for="username" class="form-label">Username</label>
                                <div class="input-group has-validation">
                                    <span class="input-group-text">@</span>
                                    <input type="text" class="form-control" id="username" placeholder="Username" required>
                                    <div class="invalid-feedback">
                                        Your username is required.
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="col-12">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="you@example.com" value="d.suseno@icubic.co.id" required>
                            <div class="invalid-feedback">
                                Please enter a valid email address for shipping updates.
                            </div>
                        </div>

                        <div class="col-12">
                            <label for="address" class="form-label">Address</label>
                            <input type="text" class="form-control" id="address" placeholder="1234 Main St" value="Pesona Citra, Blok B33" required>
                            <div class="invalid-feedback">
                                Please enter your shipping address.
                            </div>
                        </div>

                        @if( 1 == 2 )
                            <div class="col-12">
                                <label for="address2" class="form-label">Address 2 <span class="text-muted">(Optional)</span></label>
                                <input type="text" class="form-control" id="address2" placeholder="Apartment or suite" value="">
                            </div>
                        @endif

                        <div class="col-md-4">
                            <label for="province" class="form-label">Province</label>
                            <select class="form-select" id="province" required>
                                <option value="">Choose...</option>
                                <option value="Jawa Barat" selected>Jawa Barat</option>
                            </select>
                            <div class="invalid-feedback">
                                Please provide a valid province.
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label for="city" class="form-label">City</label>
                            <select class="form-select" id="city" required>
                                <option value="">Choose...</option>
                                <option value="Kab. Indramayu" selected>Kab. Indramayu</option>
                            </select>
                            <div class="invalid-feedback">
                                Please provide a valid city.
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label for="postcode" class="form-label">Postcode</label>
                            <input type="text" class="form-control" id="postcode" placeholder="" value="45215" required>
                            <div class="invalid-feedback">
                                Postcode required.
                            </div>
                        </div>
                    </div>

                    <hr class="my-4">

                    @if( 1 == 2 )
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="same-address">
                            <label class="form-check-label" for="same-address">Shipping address is the same as my billing address</label>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="save-info">
                            <label class="form-check-label" for="save-info">Save this information for next time</label>
                        </div>
                    @endif

                    <hr class="my-4">

                    <h4 class="mb-3">Payment</h4>

                    <div class="my-3">
                        <div class="form-check">
                            <input id="cc" name="paymentMethod" type="radio" class="form-check-input" value="cc" checked required>
                            <label class="form-check-label" for="cc">Credit Card</label>
                        </div>
                        <div class="form-check">
                            <input id="va" name="paymentMethod" type="radio" class="form-check-input" value="va" required>
                            <label class="form-check-label" for="va">Virtual Account</label>
                        </div>
                        <div class="form-check">
                            <input id="e-wallet-ovo" name="paymentMethod" type="radio" class="form-check-input" value="ewallet_ovo" required>
                            <label class="form-check-label" for="e-wallet-ovo">OVO</label>
                        </div>
                        <div class="form-check">
                            <input id="e-wallet-link" name="paymentMethod" type="radio" class="form-check-input" value="ewallet_link" required>
                            <label class="form-check-label" for="e-wallet-link">LinkAja</label>
                        </div>
                        <div class="form-check">
                            <input id="e-wallet-dana" name="paymentMethod" type="radio" class="form-check-input" value="ewallet_dana" required>
                            <label class="form-check-label" for="e-wallet-dana">DANA</label>
                        </div>
                        <div class="form-check">
                            <input id="e-wallet-shopeepay" name="paymentMethod" type="radio" class="form-check-input" value="ewallet_shopeepay" required>
                            <label class="form-check-label" for="e-wallet-shopeepay">Shopeepay</label>
                        </div>
                        <div class="form-check">
                            <input id="payloan-akulaku" name="paymentMethod" type="radio" class="form-check-input" value="payloan_akulaku" required>
                            <label class="form-check-label" for="payloan-akulaku">Akulaku</label>
                        </div>
                        <div class="form-check">
                            <input id="payloan-kredivo" name="paymentMethod" type="radio" class="form-check-input" value="payloan_kredivo" required>
                            <label class="form-check-label" for="payloan-kredivo">Kredivo</label>
                        </div>
                        <div class="form-check">
                            <input id="qris-shopee" name="paymentMethod" type="radio" class="form-check-input" value="qris_shopee" required>
                            <label class="form-check-label" for="qris-shopee">Shopee QRIS</label>
                        </div>
                    </div>

                    @if( 1 == 2 )
                        <div class="row gy-3">
                            <div class="col-md-6">
                                <label for="cc-name" class="form-label">Name on card</label>
                                <input type="text" class="form-control" id="cc-name" placeholder="" required>
                                <small class="text-muted">Full name as displayed on card</small>
                                <div class="invalid-feedback">
                                    Name on card is required
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label for="cc-number" class="form-label">Credit card number</label>
                                <input type="text" class="form-control" id="cc-number" placeholder="" required>
                                <div class="invalid-feedback">
                                    Credit card number is required
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label for="cc-expiration" class="form-label">Expiration</label>
                                <input type="text" class="form-control" id="cc-expiration" placeholder="" required>
                                <div class="invalid-feedback">
                                    Expiration date required
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label for="cc-cvv" class="form-label">CVV</label>
                                <input type="text" class="form-control" id="cc-cvv" placeholder="" required>
                                <div class="invalid-feedback">
                                    Security code required
                                </div>
                            </div>
                        </div>
                    @endif

                    <hr class="my-4">

                    <button class="w-100 btn btn-primary btn-lg" type="submit">Continue to checkout</button>
                </form>
            </div>
        </div>
    </main>
@endsection
